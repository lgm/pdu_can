#include "can_hal.h"
#include "device.h"
#include "driverlib.h"

#include <stdint.h>
#include <string.h>

// Pin define
#define CAN_PORT          GPIO_PORT_A
#define CAN_PORT_MASK     (0x03 << 12)
#define CAN_PIN_TX        GPIO_13_CANA_TX // GPIO13
#define CAN_PIN_RX        GPIO_12_CANA_RX // GPIO12
#define CAN_BIT_RATE      250000

#define CAN_EN_PORT       GPIO_PORT_A
#define CAN_EN_PORT_MASK  (0x01 << 11) // GPIO11
#define CAN_EN_PIN        11

#define CAN_STB_PORT      GPIO_PORT_B
#define CAN_STB_PORT_MASK (0x01 << 1) // GPIO33
#define CAN_STB_PIN       33

// Data buffer
#define TX_BUF_SIZE 168u
#define RX_BUF_SIZE 32u

// Message object define
#define MAX_OBJ_NUM      32u
#define TX_MSG_OBJ_START 1u
#define TX_MSG_OBJ_END   (TX_MSG_OBJ_START + CAN_TX_OBJ_NUM - 1u)
#define RX_MSG_OBJ_START (TX_MSG_OBJ_END + 1u)
#define RX_MSG_OBJ_END   (RX_MSG_OBJ_START + CAN_RX_OBJ_NUM - 1u)
#if (RX_MSG_OBJ_END > MAX_OBJ_NUM)
#error "Message object over range!"
#endif

typedef struct {
    uint32_t in_pos;
    uint32_t out_pos;
    uint32_t id[TX_BUF_SIZE];
    uint32_t len[TX_BUF_SIZE];
    uint16_t data[TX_BUF_SIZE][8];
} CanMsg_tx;

typedef struct {
    uint32_t in_pos;
    uint32_t out_pos;
    uint32_t id[RX_BUF_SIZE];
    uint16_t data[RX_BUF_SIZE][8];
} CanMsg_rx;

static uint32_t RX_MSG_ID[CAN_RX_OBJ_NUM] = {
    CAN_ID_RX_HALL_LEFT_BAT,
    CAN_ID_RX_HALL_CENT_BAT,
    CAN_ID_RX_HALL_RIGHT_BAT,
    CAN_ID_RX_HALL_X4,
    CAN_ID_RX_HALL_X5,
    CAN_ID_RX_HALL_X6,
    CAN_ID_RX_HALL_X7,
    CAN_ID_RX_HALL_X8,
    CAN_ID_RX_HALL_RANGE_EXTENDER,
    CAN_ID_RX_HALL_FAST_CHRG_A,
    CAN_ID_RX_HALL_FAST_CHRG_B,
    CAN_ID_RX_HALL_OBC,
    CAN_ID_RX_RELAY_CTRL_VCU,
    CAN_ID_RX_RELAY_CTRL_CHRG,
#ifdef PDU_UP_CAR
    CAN_ID_DCAR_RELAY_STATE,
#else
    0x1800FF00u, // dummy ID, down car is only to send
#endif
    CAN_ID_FW_WCT,
    CAN_ID_FW_PHYSICS,
    CAN_ID_FW_FUN,
    CAN_ID_RX_TEMP_DETECT,
};

static CAN_MsgFrameType RX_MSG_FRAM_TYPE[CAN_RX_OBJ_NUM] = {
    CAN_MSG_FRAME_STD, CAN_MSG_FRAME_STD, CAN_MSG_FRAME_STD, CAN_MSG_FRAME_STD,
    CAN_MSG_FRAME_STD, CAN_MSG_FRAME_STD, CAN_MSG_FRAME_STD, CAN_MSG_FRAME_STD,
    CAN_MSG_FRAME_STD, CAN_MSG_FRAME_STD, CAN_MSG_FRAME_STD, CAN_MSG_FRAME_STD,
    CAN_MSG_FRAME_EXT, CAN_MSG_FRAME_EXT, CAN_MSG_FRAME_EXT, CAN_MSG_FRAME_STD,
    CAN_MSG_FRAME_STD, CAN_MSG_FRAME_STD, CAN_MSG_FRAME_EXT,
};

static CanMsg_tx s_tx_msg;
static CanMsg_rx s_rx_msg;

static void GpioConfig(void);
static void CanConfig(void);
static void ReceiveMsg(void);
static void TransmitMsg(void);

void CAN_HAL_Init(void)
{
    memset(&s_tx_msg, 0, sizeof(s_tx_msg));
    memset(&s_rx_msg, 0, sizeof(s_rx_msg));
    GpioConfig();
    CanConfig();
}

void CAN_HAL_TaskIsr(void)
{
    ReceiveMsg();
    TransmitMsg();
}

bool CAN_HAL_SendMsg(uint32_t id, const uint16_t *data, uint32_t len)
{
    bool ret = false;
    if (s_tx_msg.in_pos == s_tx_msg.out_pos) {
        s_tx_msg.in_pos = 0u;
        s_tx_msg.out_pos = 0u;
    }
    if (len <= 8u) {
        if (s_tx_msg.in_pos < TX_BUF_SIZE) {
            s_tx_msg.id[s_tx_msg.in_pos] = id;
            s_tx_msg.len[s_tx_msg.in_pos] = len;
            uint32_t i;
            for (i = 0u; i < len; i++) {
                s_tx_msg.data[s_tx_msg.in_pos][i] = data[i];
            }
            s_tx_msg.in_pos++;
            ret = true;
        }
    }
    return ret;
}

bool CAN_HAL_IsMsgPending(void)
{
    return (s_rx_msg.in_pos != s_rx_msg.out_pos);
}

CAN_Msg CAN_HAL_ReadMsg(void)
{
    CAN_Msg msg;
    msg.id = s_rx_msg.id[s_rx_msg.out_pos];
    uint32_t i;
    for (i = 0u; i < 8u; i++) {
        msg.data[i] = s_rx_msg.data[s_rx_msg.out_pos][i];
    }
    s_rx_msg.out_pos = (s_rx_msg.out_pos + 1) % RX_BUF_SIZE;
    return msg;
}

static void GpioConfig(void)
{
    // Can Tx and Rx
    GPIO_unlockPortConfig(CAN_PORT, CAN_PORT_MASK);
    GPIO_setPinConfig(CAN_PIN_TX);
    GPIO_setPinConfig(CAN_PIN_RX);
    // Can Enable
    GPIO_unlockPortConfig(CAN_EN_PORT, CAN_EN_PORT_MASK);
    GPIO_setPadConfig(CAN_EN_PIN, GPIO_PIN_TYPE_STD);
    GPIO_setDirectionMode(CAN_EN_PIN, GPIO_DIR_MODE_OUT);
    GPIO_writePin(CAN_EN_PIN, 1u); // Default enable can
    //    // Can stanby
    GPIO_unlockPortConfig(CAN_STB_PORT, CAN_STB_PORT_MASK);
    GPIO_setPadConfig(CAN_STB_PIN, GPIO_PIN_TYPE_STD);
    GPIO_setDirectionMode(CAN_STB_PIN, GPIO_DIR_MODE_OUT);
    GPIO_writePin(CAN_STB_PIN, 1u); // Default normal mode, active LOW
}

static void CanConfig(void)
{
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_CANA);

    CAN_initModule(CANA_BASE);
    CAN_setBitRate(CANA_BASE, DEVICE_SYSCLK_FREQ, CAN_BIT_RATE, 20);
    // Tx message object config
    uint32_t obj_id = TX_MSG_OBJ_START;
    for (obj_id = TX_MSG_OBJ_START; obj_id <= TX_MSG_OBJ_END; ++obj_id) {
        CAN_setupMessageObject(CANA_BASE,
                               obj_id,
                               0u,
                               CAN_MSG_FRAME_EXT,
                               CAN_MSG_OBJ_TYPE_TX,
                               0u,
                               CAN_MSG_OBJ_NO_FLAGS,
                               8u);
    }
    // Rx message object config
    for (obj_id = RX_MSG_OBJ_START; obj_id <= RX_MSG_OBJ_END; ++obj_id) {
        uint32_t msk = 0x1FFFFFFFu;
        if (obj_id == RX_MSG_OBJ_END) { // The temp can ID should ignore last
                                        // 8 bits
            msk = 0x1FFFFF00u;
        }
        CAN_setupMessageObject(CANA_BASE,
                               obj_id,
                               RX_MSG_ID[obj_id - RX_MSG_OBJ_START],
                               RX_MSG_FRAM_TYPE[obj_id - RX_MSG_OBJ_START],
                               CAN_MSG_OBJ_TYPE_RX,
                               msk,
                               (CAN_MSG_OBJ_USE_ID_FILTER | CAN_MSG_OBJ_NO_FLAGS
                                | CAN_MSG_OBJ_USE_EXT_FILTER),
                               8u);
    }

    CAN_startModule(CANA_BASE);
    CAN_enableAutoBusOn(CANA_BASE);
}

static void ReceiveMsg(void)
{
    uint32_t msg_flag = CAN_getNewDataFlags(CANA_BASE);
    if (msg_flag != 0u) {
        uint32_t msg_obj = 0u;
        uint32_t i;
        for (i = RX_MSG_OBJ_END; i >= RX_MSG_OBJ_START; i--) {
            if ((msg_flag & ((uint32_t)1u << (i - 1u))) != 0u) {
                msg_obj = i;
                break;
            }
        }
        if (msg_obj != 0u) {
            CAN_MsgFrameType frame_type; // Fix extened frame, so ignore this
            (void)CAN_readMessageWithID(CANA_BASE,
                                        msg_obj,
                                        &frame_type,
                                        &s_rx_msg.id[s_rx_msg.in_pos],
                                        s_rx_msg.data[s_rx_msg.in_pos]);
            s_rx_msg.in_pos = (s_rx_msg.in_pos + 1) % RX_BUF_SIZE;
        }
    }
}

static void TransmitMsg(void)
{
    if (s_tx_msg.out_pos != s_tx_msg.in_pos) {
        uint32_t msg_obj = 0u;
        uint32_t tx_request = CAN_getTxRequests(CANA_BASE);
        uint32_t i;
        for (i = TX_MSG_OBJ_START; i <= TX_MSG_OBJ_END; i++) {
            if ((tx_request & ((uint32_t)1u << (i - 1u))) == 0u) {
                msg_obj = i;
                break;
            }
        }
        if (msg_obj != 0u) {
            CAN_setupMessageObject(CANA_BASE,
                                   msg_obj,
                                   s_tx_msg.id[s_tx_msg.out_pos],
                                   CAN_MSG_FRAME_EXT,
                                   CAN_MSG_OBJ_TYPE_TX,
                                   0u,
                                   CAN_MSG_OBJ_NO_FLAGS,
                                   8u);
            CAN_sendMessage(CANA_BASE,
                            msg_obj,
                            s_tx_msg.len[s_tx_msg.out_pos],
                            s_tx_msg.data[s_tx_msg.out_pos]);
            s_tx_msg.out_pos = (s_tx_msg.out_pos + 1) % TX_BUF_SIZE;
        }
    }
}
