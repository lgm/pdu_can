#ifndef CAN_INTERNAL_H_
#define CAN_INTERNAL_H_

#include "target_config.h"

#include <stdint.h>

#define CAN_TX_OBJ_NUM 6u
#define CAN_RX_OBJ_NUM 19u

// Send data ID config
// 100ms
#define CAN_ID_TX_SAMPLE_INFO_1   0x1800D0F3u
#define CAN_ID_TX_SAMPLE_INFO_2   0x1801D0F3u
#define CAN_ID_TX_RELAY_STATE     0x1802D0F3u
#define CAN_ID_TX_ALARMS          0x1810D0F3u
#define CAN_ID_TX_BAT_SYSTEM_INFO 0x1820D0F3u
// 1000ms
#define CAN_ID_TX_BAY_SYSTEM_VOLT 0x1822D0F3u // 0x1822D0F3u~1866D0F3
#define CAN_ID_TX_BAT_SYSTEM_TEMP 0x1867D0F3u // 0x1867D0F3u~1889D0F3
#define CAN_ID_TX_BAT_VOLT_INFO   0x1890D0F3u
#define CAN_ID_TX_BAT_TEMP_INFO   0x1891D0F3u

// Receive data ID Config
// Hall, 30ms
#define CAN_ID_RX_HALL_LEFT_BAT       0x3C0u
#define CAN_ID_RX_HALL_CENT_BAT       0x3C1u
#define CAN_ID_RX_HALL_RIGHT_BAT      0x3C2u
#define CAN_ID_RX_HALL_X4             0x3C3u
#define CAN_ID_RX_HALL_X5             0x3C4u
#define CAN_ID_RX_HALL_X6             0x3C5u
#define CAN_ID_RX_HALL_X7             0x3C6u
#define CAN_ID_RX_HALL_X8             0x3C7u
#define CAN_ID_RX_HALL_RANGE_EXTENDER 0x3C8u
#define CAN_ID_RX_HALL_FAST_CHRG_A    0x3C9u
#define CAN_ID_RX_HALL_FAST_CHRG_B    0x3CAu
#define CAN_ID_RX_HALL_OBC            0x3CBu
#define CAN_ID_RX_HALL_START_ID       CAN_ID_RX_HALL_LEFT_BAT
// Relay control, 100ms
#define CAN_ID_RX_RELAY_CTRL_VCU  0x1800F3D0u
#define CAN_ID_RX_RELAY_CTRL_CHRG 0x1806D0F4u
// Down car relay state, for down car to up car
#define CAN_ID_DCAR_RELAY_STATE 0x1803D0F3u
// Firmware update
#define CAN_ID_FW_WCT     0x73Bu
#define CAN_ID_FW_PHYSICS 0x733u
#define CAN_ID_FW_FUN     0x7DFu
// Temp detect, 1000ms
#define CAN_ID_RX_TEMP_DETECT 0x18ECF400u // 0x18ECF4xxu

typedef struct {
    uint32_t id;
    uint16_t data[8];
} CAN_Msg;

typedef uint16_t HALL_Curr;
enum {
    HALL_CURR_LEFT_BAT,
    HALL_CURR_CENT_BAT,
    HALL_CURR_RIGHT_BAT,
    HALL_CURR_X4,
    HALL_CURR_X5,
    HALL_CURR_X6,
    HALL_CURR_X7,
    HALL_CURR_X8,
    HALL_CURR_RANGE_EXTENDER,
    HALL_CURR_FAST_CHRG_A,
    HALL_CURR_FAST_CHRG_B,
    HALL_CURR_OBC,
    HALL_CURR_END
};
#define HALL_CURR_NUM ((HALL_Curr)HALL_CURR_END)

typedef struct {
    uint16_t x7_x8 : 1; // Main circuit
    uint16_t range_extender : 1; // Range extender
    uint16_t air_condition : 1; // Air-conditioning
    uint16_t fast_chrg_a : 1;
    uint16_t fast_chrg_b : 1;
    uint16_t obc : 1;
    uint16_t reseve : 10;
} RELAY_Ctrl;

typedef struct {
    uint16_t x7_x8 : 2; // Main circuit
    uint16_t range_extender : 2; // Range extender
    uint16_t obc : 2;
    uint16_t fast_chrg_a : 2;
    uint16_t fast_chrg_b : 2;
    uint16_t reseve : 6;
    uint16_t time_stamp : 8;
    uint16_t reseve2 : 8;
} DCAR_RELAY_State;

typedef struct {
    int16_t left_value;
    int16_t center_value;
    int16_t right_value;
} BAT_Temp;

typedef struct {
    uint16_t bus_volt;
    int16_t bus_curr;
    int16_t x7_curr;
    int16_t x8_curr;
} SAMPLE_Info1;

typedef struct {
    int16_t range_extender_curr;
    int16_t obc_curr;
    int16_t fast_a_curr;
    int16_t fast_b_curr;
} SAMPLE_Info2;

typedef struct {
    uint16_t left_bat : 2;
    uint16_t center_bat : 2;
    uint16_t right_bat : 2;
    uint16_t air_condition : 2;
    uint16_t x7_x8 : 2;
    uint16_t range_extender : 2;
    uint16_t obc : 2;
    uint16_t fast_a : 2;
    uint16_t fast_b : 2;
    uint16_t reseve : 6;
    uint16_t time_stamp : 8;
} RELAY_State;

typedef struct {
    uint16_t alarm_level : 2;
    uint16_t over_volt : 2;
    uint16_t under_volt : 2;
    uint16_t bat_volt_diff : 2;
    uint16_t hvil : 2;
    uint16_t bat_uniformity : 2;
    uint16_t left_bat_ov : 2;
    uint16_t center_bat_ov : 2;
    uint16_t right_bat_ov : 2;
    uint16_t left_bat_uv : 2;
    uint16_t center_bat_uv : 2;
    uint16_t right_bat_uv : 2;
    uint16_t left_bat_ut : 2;
    uint16_t center_bat_ut : 2;
    uint16_t right_bat_ut : 2;
    uint16_t left_bat_ot : 2;
    uint16_t center_bat_ot : 2;
    uint16_t right_bat_ot : 2;
    uint16_t left_bat_vd : 2;
    uint16_t center_bat_vd : 2;
    uint16_t right_bat_vd : 2;
    uint16_t left_bat_td : 2;
    uint16_t center_bat_td : 2;
    uint16_t right_bat_td : 2;
    uint16_t hv_request : 1;
    uint16_t reseve : 15;
} ALARM_Info;

typedef struct {
    uint16_t volt;
    int16_t curr;
} BAT_SYS_info;

typedef struct {
    uint16_t high_index : 8;
    uint16_t high_value;
    uint16_t low_index : 8;
    uint16_t low_value;
} BAT_VOLT_Info;

typedef struct {
    uint16_t high_index : 8;
    int16_t high_value : 8;
    uint16_t low_index : 8;
    int16_t low_value : 8;
} BAT_TEMP_Info;

#endif /* CAN_INTERNAL_H_ */
