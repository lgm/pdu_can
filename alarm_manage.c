#include "alarm_manage.h"

#include "can_app.h"

#include <stdbool.h>
#include <string.h>

typedef int AID;
enum {
    AID_LEVEL = 0,
    AID_OV, // Over voltage
    AID_UV, // Under voltage
    AID_VD, // Voltage diff
    AID_HVIL, // High voltage interlock
    AID_UNIFORMITY, // Uniformity
    AID_LEFT_BAT_OV, // Battery over voltage
    AID_CENTER_BAT_OV,
    AID_RIGHT_BAT_OV,
    AID_LEFT_BAT_UV, // Battery under voltage
    AID_CENTER_BAT_UV,
    AID_RIGHT_BAT_UV,
    AID_LEFT_BAT_UT, // Battery under temperature
    AID_CENTER_BAT_UT,
    AID_RIGHT_BAT_UT,
    AID_LEFT_BAT_OT, // Battery over temperature
    AID_CENTER_BAT_OT,
    AID_RIGHT_BAT_OT,
    AID_LEFT_BAT_VD, // Battery voltage diff
    AID_CENTER_BAT_VD,
    AID_RIGHT_BAT_VD,
    AID_LEFT_BAT_TD, // Temperature diff
    AID_CENTER_BAT_TD,
    AID_RIGHT_BAT_TD,
    AID_HV_REQUEST,
    AID_END
};
#define AID_NUM ((AID)AID_END)

typedef enum {
    ALARM_NONE = 0,
    ALARM_LEVEL1 = 1,
    ALARM_LEVEL2,
    ALARM_LEVEL3,
} ALARM_LEVEL;

typedef struct {
    uint32_t level1;
    uint32_t level2;
    uint32_t level3;
} ALARM_TIME;

typedef struct {
    int16_t level1;
    int16_t level2;
    int16_t level3;
} ALRM_VALUE;

static const ALARM_TIME FAULT_TIME_TOLERANCE[AID_NUM] = {
    [AID_LEVEL] = { 1000u, 1000u, 500u }, // fixed
    [AID_OV] = { 1000u, 1000u, 500u },
    [AID_UV] = { 1000u, 1000u, 1000u },
    [AID_VD] = { 1000u, 1000u, 1000u },
    [AID_HVIL] = { 1000u, 1000u, 1000u },
    [AID_UNIFORMITY] = { 1000u, 1000u, 1000u },
    [AID_LEFT_BAT_OV] = { 1000u, 1000u, 500u },
    [AID_CENTER_BAT_OV] = { 1000u, 1000u, 500u },
    [AID_RIGHT_BAT_OV] = { 1000u, 1000u, 500u },
    [AID_LEFT_BAT_UV] = { 1000u, 1000u, 1000u },
    [AID_CENTER_BAT_UV] = { 1000u, 1000u, 1000u },
    [AID_RIGHT_BAT_UV] = { 1000u, 1000u, 1000u },
    [AID_LEFT_BAT_UT] = { 1000u, 1000u, 1000u },
    [AID_CENTER_BAT_UT] = { 1000u, 1000u, 1000u },
    [AID_RIGHT_BAT_UT] = { 1000u, 1000u, 1000u },
    [AID_LEFT_BAT_OT] = { 1000u, 1000u, 1000u },
    [AID_CENTER_BAT_OT] = { 1000u, 1000u, 1000u },
    [AID_RIGHT_BAT_OT] = { 1000u, 1000u, 1000u },
    [AID_LEFT_BAT_VD] = { 1000u, 1000u, 1000u },
    [AID_CENTER_BAT_VD] = { 1000u, 1000u, 1000u },
    [AID_RIGHT_BAT_VD] = { 1000u, 1000u, 1000u },
    [AID_LEFT_BAT_TD] = { 5000u, 5000u, 5000u },
    [AID_CENTER_BAT_TD] = { 5000u, 5000u, 5000u },
    [AID_RIGHT_BAT_TD] = { 5000u, 5000u, 5000u },
    [AID_HV_REQUEST] = { 1000u, 1000u, 1000u }, // fixed
};

static const ALRM_VALUE ALARM_VALUE_TOLERANCE[AID_NUM] = {
    [AID_LEVEL] = { 710, 715, 718 }, // ignore
    [AID_OV] = { 705, 710, 715 }, // The unit is 1V
    [AID_UV] = { 481, 464, 440 }, // The unit is 1V
    [AID_VD] = { 3, 5, 8 },
    [AID_HVIL] = { 3, 5, 8 },
    [AID_UNIFORMITY] = { 100, 120, 150 }, // The unit is mV
    [AID_LEFT_BAT_OV] = { 705, 710, 718 },
    [AID_CENTER_BAT_OV] = { 705, 710, 718 },
    [AID_RIGHT_BAT_OV] = { 705, 710, 718 },
    [AID_LEFT_BAT_UV] = { 481, 464, 440 },
    [AID_CENTER_BAT_UV] = { 481, 464, 440 },
    [AID_RIGHT_BAT_UV] = { 481, 464, 440 },
    [AID_LEFT_BAT_UT] = { -15, -25, -40 }, // The unit is 1C
    [AID_CENTER_BAT_UT] = { -15, -25, -40 },
    [AID_RIGHT_BAT_UT] = { -15, -25, -40 },
    [AID_LEFT_BAT_OT] = { 50, 55, 60 },
    [AID_CENTER_BAT_OT] = { 50, 55, 60 },
    [AID_RIGHT_BAT_OT] = { 50, 55, 60 },
    [AID_LEFT_BAT_VD] = { 3, 5, 8 },
    [AID_CENTER_BAT_VD] = { 3, 5, 8 },
    [AID_RIGHT_BAT_VD] = { 3, 5, 8 },
    [AID_LEFT_BAT_TD] = { 5, 10, 15 },
    [AID_CENTER_BAT_TD] = { 5, 10, 15 },
    [AID_RIGHT_BAT_TD] = { 5, 10, 15 },
    [AID_HV_REQUEST] = { 0, 0, 0 }, // ignore
};

static const ALRM_VALUE RESTORE_VALUE_HYST[AID_NUM] = {
    [AID_LEVEL] = { -3, -3, -3 }, // ignore
    [AID_OV] = { -3, -3, -3 }, // The unit is 1V
    [AID_UV] = { -3, -3, -10 }, // The unit is 1V
    [AID_VD] = { -3, -1, -2 },
    [AID_HVIL] = { -3, -1, -2 },
    [AID_UNIFORMITY] = { -30, -10, -20 }, // The unit is mV
    [AID_LEFT_BAT_OV] = { -3, -3, -3 },
    [AID_CENTER_BAT_OV] = { -3, -3, -3 },
    [AID_RIGHT_BAT_OV] = { -3, -3, -3 },
    [AID_LEFT_BAT_UV] = { -3, -3, -10 },
    [AID_CENTER_BAT_UV] = { -3, -3, -10 },
    [AID_RIGHT_BAT_UV] = { -3, -3, -10 },
    [AID_LEFT_BAT_UT] = { 3, 3, 3 }, // The unit is 1C
    [AID_CENTER_BAT_UT] = { 3, 3, 3 },
    [AID_RIGHT_BAT_UT] = { 3, 3, 3 },
    [AID_LEFT_BAT_OT] = { -3, -3, -3 },
    [AID_CENTER_BAT_OT] = { -3, -3, -3 },
    [AID_RIGHT_BAT_OT] = { -3, -3, -3 },
    [AID_LEFT_BAT_VD] = { -3, -1, -2 },
    [AID_CENTER_BAT_VD] = { -3, -1, -2 },
    [AID_RIGHT_BAT_VD] = { -3, -1, -2 },
    [AID_LEFT_BAT_TD] = { -2, -2, -2 },
    [AID_CENTER_BAT_TD] = { 2, -2, -2 },
    [AID_RIGHT_BAT_TD] = { 2, -2, -2 },
    [AID_HV_REQUEST] = { 0, 0, 0 }, // ignore
};

static ALARM_Code s_alarm_code;
static ALARM_Code s_pre_alarm_code;
static ALARM_TIME s_fault_time[AID_NUM];
static ALARM_LEVEL s_max_level = ALARM_NONE;

static void CheckBus(void);
static void CheckLeftBat(void);
static void CheckCenterBat(void);
static void CheckRightBat(void);
static void CheckOthers(void);
static void CheckOverProtectValue(AID AID, int16_t value);
static void CheckUnderProtectValue(AID AID, int16_t value);
static void UpdateFaultTimer(AID fid, bool fault_happening, ALARM_LEVEL level);
static ALARM_LEVEL GetPreAlarmLevel(AID aid);
static void OnFault(AID aid, ALARM_LEVEL level);

void AM_Init(void)
{
    memset(&s_alarm_code, 0, sizeof(s_alarm_code));
    memset(&s_pre_alarm_code, 0, sizeof(s_pre_alarm_code));
    memset(s_fault_time, 0, sizeof(s_fault_time));
}

void AM_Task1ms(void)
{
    CheckBus();
    CheckLeftBat();
    CheckCenterBat();
    CheckRightBat();
    CheckOthers();
}

ALARM_Code AM_GetAndClearAlarmCode(void)
{
    ALARM_Code code = s_alarm_code;
    s_pre_alarm_code = s_alarm_code;
    memset(&s_alarm_code, 0, sizeof(s_alarm_code));
    s_max_level = ALARM_NONE;
    return code;
}

static void CheckBus(void)
{
    // Check bus voltage
    int16_t bus_volt = (int16_t)CAN_APP_GetSampleResult(AIN_BUS_VOLT) / 10;
    CheckOverProtectValue(AID_OV, bus_volt);
    CheckUnderProtectValue(AID_UV, bus_volt);
    // Check bus vol diff, disable it, because the sample precision is too low
    // int16_t bat_left_volt =
    //         (int16_t)CAN_APP_GetSampleResult(AIN_LEFT_BAT_VOLT) / 10;
    // int16_t bat_center_volt =
    //         (int16_t)CAN_APP_GetSampleResult(AIN_CENTER_BAT_VOLT) / 10;
    // int16_t bat_right_volt =
    //         (int16_t)CAN_APP_GetSampleResult(AIN_RANGE_EXTENDER_VOLT) / 10;
    // int16_t max_volt = bat_left_volt;
    // int16_t min_volt = bat_left_volt;
    // if (bat_center_volt > max_volt) {
    //     max_volt = bat_center_volt;
    // }
    // if (bat_right_volt > max_volt) {
    //     max_volt = bat_right_volt;
    // }
    // if (bat_center_volt < min_volt) {
    //     min_volt = bat_center_volt;
    // }
    // if (bat_right_volt < min_volt) {
    //     min_volt = bat_right_volt;
    // }
    // int16_t bus_vd = max_volt - min_volt;
    // CheckOverProtectValue(AID_VD, bus_vd);
}

static void CheckLeftBat(void)
{
    int16_t bat_volt = (int16_t)CAN_APP_GetSampleResult(AIN_LEFT_BAT_VOLT) / 10;
    CheckOverProtectValue(AID_LEFT_BAT_OV, bat_volt);
    CheckUnderProtectValue(AID_LEFT_BAT_UV, bat_volt);
    int16_t bat_temp = CAN_APP_GetBatTemp().left_value;
    CheckOverProtectValue(AID_LEFT_BAT_OT, bat_temp);
    CheckUnderProtectValue(AID_LEFT_BAT_UT, bat_temp);
    // int16_t bat_vd = 0; // Can't get it, so set it to 0
    // CheckOverProtectValue(AID_LEFT_BAT_VD, bat_vd);
    // int16_t bat_td = 0u; // Can't get it, so set it to 0
    // CheckOverProtectValue(AID_LEFT_BAT_TD, bat_td);
}

static void CheckCenterBat(void)
{
    int16_t bat_volt =
            (int16_t)CAN_APP_GetSampleResult(AIN_CENTER_BAT_VOLT) / 10;
    CheckOverProtectValue(AID_CENTER_BAT_OV, bat_volt);
    CheckUnderProtectValue(AID_CENTER_BAT_UV, bat_volt);
    int16_t bat_temp = CAN_APP_GetBatTemp().center_value;
    CheckOverProtectValue(AID_CENTER_BAT_OT, bat_temp);
    CheckUnderProtectValue(AID_CENTER_BAT_UT, bat_temp);
    // int16_t bat_vd = 0; // Can't get it, so set it to 0
    // CheckOverProtectValue(AID_CENTER_BAT_VD, bat_vd);
    // int16_t bat_td = 0u; // Can't get it, so set it to 0
    // CheckOverProtectValue(AID_CENTER_BAT_TD, bat_td);
}

static void CheckRightBat(void)
{
    int16_t bat_volt =
            (int16_t)CAN_APP_GetSampleResult(AIN_RIGHT_BAT_VOLT) / 10;
    CheckOverProtectValue(AID_RIGHT_BAT_OV, bat_volt);
    CheckUnderProtectValue(AID_RIGHT_BAT_UV, bat_volt);
    int16_t bat_temp = CAN_APP_GetBatTemp().right_value;
    CheckOverProtectValue(AID_RIGHT_BAT_OT, bat_temp);
    CheckUnderProtectValue(AID_RIGHT_BAT_UT, bat_temp);
    // int16_t bat_vd = 0; // Can't get it, so set it to 0
    // CheckOverProtectValue(AID_RIGHT_BAT_VD, bat_vd);
    // int16_t bat_td = 0; // Can't get it, so set it to 0
    // CheckOverProtectValue(AID_RIGHT_BAT_TD, bat_td);
}

static void CheckOthers(void)
{
    // Check bat uniformity, but can't get it, so set it to 0
    // int16_t uniformity = 0;
    // CheckOverProtectValue(AID_UNIFORMITY, uniformity);
    // Check HVIL
    int16_t hvil = CAN_APP_GetHVILState();
    if (hvil == 1) {
        OnFault(AID_HVIL, ALARM_LEVEL1);
    }
    // Check HV request
    int16_t hv_request = (int16_t)CAN_APP_GetHVRequestState();
    if (hv_request == 1) {
        OnFault(AID_HV_REQUEST, ALARM_LEVEL1);
    }
    // Set max level, it must be called after all fault check
    if (s_max_level != ALARM_NONE) {
        OnFault(AID_LEVEL, s_max_level);
    }
}

static void CheckOverProtectValue(AID AID, int16_t value)
{
    ALARM_LEVEL curr_level = GetPreAlarmLevel(AID);
    if (curr_level == ALARM_NONE) {
        if (value > ALARM_VALUE_TOLERANCE[AID].level3) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL3);
        } else if (value > ALARM_VALUE_TOLERANCE[AID].level2) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL2);
        } else if (value > ALARM_VALUE_TOLERANCE[AID].level1) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL1);
        } else {
            UpdateFaultTimer(AID, false, ALARM_NONE);
        }
    } else if (curr_level == ALARM_LEVEL1) {
        if (value > ALARM_VALUE_TOLERANCE[AID].level3) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL3);
        } else if (value > ALARM_VALUE_TOLERANCE[AID].level2) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL2);
        } else if (value < (ALARM_VALUE_TOLERANCE[AID].level1
                            + RESTORE_VALUE_HYST[AID].level1)) {
            UpdateFaultTimer(AID, false, ALARM_NONE);
        } else {
            UpdateFaultTimer(AID, true, ALARM_LEVEL1);
        }
    } else if (curr_level == ALARM_LEVEL2) {
        if (value > ALARM_VALUE_TOLERANCE[AID].level3) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL3);
        } else if (value < ALARM_VALUE_TOLERANCE[AID].level1) {
            UpdateFaultTimer(AID, false, ALARM_NONE);
        } else if (value < (ALARM_VALUE_TOLERANCE[AID].level2
                            + RESTORE_VALUE_HYST[AID].level2)) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL1);
        } else {
            UpdateFaultTimer(AID, true, ALARM_LEVEL2);
        }
    } else if (curr_level == ALARM_LEVEL3) {
        if (value < ALARM_VALUE_TOLERANCE[AID].level1) {
            UpdateFaultTimer(AID, false, ALARM_NONE);
        } else if (value < ALARM_VALUE_TOLERANCE[AID].level2) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL1);
        } else if (value < (ALARM_VALUE_TOLERANCE[AID].level3
                            + RESTORE_VALUE_HYST[AID].level3)) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL2);
        } else {
            UpdateFaultTimer(AID, true, ALARM_LEVEL3);
        }
    } else {
        // Do nothing
    }
}

static void CheckUnderProtectValue(AID AID, int16_t value)
{
    ALARM_LEVEL curr_level = GetPreAlarmLevel(AID);
    if (curr_level == ALARM_NONE) {
        if (value < ALARM_VALUE_TOLERANCE[AID].level3) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL3);
        } else if (value < ALARM_VALUE_TOLERANCE[AID].level2) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL2);
        } else if (value < ALARM_VALUE_TOLERANCE[AID].level1) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL1);
        } else {
            UpdateFaultTimer(AID, false, ALARM_NONE);
        }
    } else if (curr_level == ALARM_LEVEL1) {
        if (value < ALARM_VALUE_TOLERANCE[AID].level3) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL3);
        } else if (value < ALARM_VALUE_TOLERANCE[AID].level2) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL2);
        } else if (value > (ALARM_VALUE_TOLERANCE[AID].level1
                            + RESTORE_VALUE_HYST[AID].level1)) {
            UpdateFaultTimer(AID, false, ALARM_NONE);
        } else {
            UpdateFaultTimer(AID, true, ALARM_LEVEL1);
        }
    } else if (curr_level == ALARM_LEVEL2) {
        if (value < ALARM_VALUE_TOLERANCE[AID].level3) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL3);
        } else if (value > ALARM_VALUE_TOLERANCE[AID].level1) {
            UpdateFaultTimer(AID, false, ALARM_NONE);
        } else if (value > (ALARM_VALUE_TOLERANCE[AID].level2
                            + RESTORE_VALUE_HYST[AID].level2)) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL1);
        } else {
            UpdateFaultTimer(AID, true, ALARM_LEVEL2);
        }
    } else if (curr_level == ALARM_LEVEL3) {
        if (value > ALARM_VALUE_TOLERANCE[AID].level1) {
            UpdateFaultTimer(AID, false, ALARM_NONE);
        } else if (value > ALARM_VALUE_TOLERANCE[AID].level2) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL1);
        } else if (value > (ALARM_VALUE_TOLERANCE[AID].level3
                            + RESTORE_VALUE_HYST[AID].level3)) {
            UpdateFaultTimer(AID, true, ALARM_LEVEL2);
        } else {
            UpdateFaultTimer(AID, true, ALARM_LEVEL3);
        }
    } else {
        // Do nothing
    }
}

static void UpdateFaultTimer(AID fid, bool fault_happening, ALARM_LEVEL level)
{
    if (fault_happening) {
        if (level == ALARM_LEVEL1) {
            if (s_fault_time[fid].level1
                < (2u * FAULT_TIME_TOLERANCE[fid].level1)) {
                s_fault_time[fid].level1++;
            }
            if (s_fault_time[fid].level2 > 0u) {
                s_fault_time[fid].level2--;
            }
            if (s_fault_time[fid].level3 > 0u) {
                s_fault_time[fid].level3--;
            }
            if (s_fault_time[fid].level1 >= FAULT_TIME_TOLERANCE[fid].level1) {
                OnFault(fid, ALARM_LEVEL1);
            }
        } else if (level == ALARM_LEVEL2) {
            if (s_fault_time[fid].level1
                < (2u * FAULT_TIME_TOLERANCE[fid].level1)) {
                s_fault_time[fid].level1++;
            }
            if (s_fault_time[fid].level2
                < (2u * FAULT_TIME_TOLERANCE[fid].level2)) {
                s_fault_time[fid].level2++;
            }
            if (s_fault_time[fid].level3 > 0u) {
                s_fault_time[fid].level3--;
            }
            if (s_fault_time[fid].level2 >= FAULT_TIME_TOLERANCE[fid].level2) {
                OnFault(fid, ALARM_LEVEL2);
            } else if (s_fault_time[fid].level1
                       >= FAULT_TIME_TOLERANCE[fid].level1) {
                OnFault(fid, ALARM_LEVEL1);
            } else {
                // Do nothing
            }
        } else if (level == ALARM_LEVEL3) {
            if (s_fault_time[fid].level1
                < (2u * FAULT_TIME_TOLERANCE[fid].level1)) {
                s_fault_time[fid].level1++;
            }
            if (s_fault_time[fid].level2
                < (2u * FAULT_TIME_TOLERANCE[fid].level2)) {
                s_fault_time[fid].level2++;
            }
            if (s_fault_time[fid].level3
                < (2u * FAULT_TIME_TOLERANCE[fid].level3)) {
                s_fault_time[fid].level3++;
            }
            if (s_fault_time[fid].level3 >= FAULT_TIME_TOLERANCE[fid].level3) {
                OnFault(fid, ALARM_LEVEL3);
            } else if (s_fault_time[fid].level2
                       >= FAULT_TIME_TOLERANCE[fid].level2) {
                OnFault(fid, ALARM_LEVEL2);
            } else if (s_fault_time[fid].level1
                       >= FAULT_TIME_TOLERANCE[fid].level1) {
                OnFault(fid, ALARM_LEVEL1);
            } else {
                // Do nothing
            }
        } else {
            // Do nothing
        }
    } else {
        if (s_fault_time[fid].level1 > 0u) {
            s_fault_time[fid].level1--;
        }
        if (s_fault_time[fid].level2 > 0u) {
            s_fault_time[fid].level2--;
        }
        if (s_fault_time[fid].level3 > 0u) {
            s_fault_time[fid].level3--;
        }
    }
}

static ALARM_LEVEL GetPreAlarmLevel(AID aid)
{
    ALARM_LEVEL ret = ALARM_NONE;
    if (aid < 16) {
        ret = (ALARM_LEVEL)((s_pre_alarm_code.low >> (aid * 2u)) & 0x3u);
    } else if (aid < 32) {
        ret = (ALARM_LEVEL)((s_pre_alarm_code.high >> ((aid - 16u) * 2u))
                            & 0x3u);
    } else {
        ret = ALARM_NONE;
    }
    return ret;
}

static void OnFault(AID aid, ALARM_LEVEL level)
{
    if (level <= ALARM_LEVEL3) {
        if (aid < 16) {
            uint32_t mask = ~((uint32_t)0x3u << (aid * 2u));
            mask &= s_alarm_code.low; // clear fault bits for this aid
            s_alarm_code.low = mask | ((uint32_t)level << (aid * 2u));
        } else if (aid < 32) {
            uint32_t mask = ~((uint32_t)0x3u << ((aid - 16u) * 2u));
            mask &= s_alarm_code.high; // clear fault bits for this aid
            s_alarm_code.high |= ((uint32_t)level << ((aid - 16u) * 2u));
        } else {
            // Do nothing
        }
#if 1
        // Get max level
        if (level > s_max_level) {
            s_max_level = level;
        }
#endif
    }
}
