#ifndef CAN_HAL_H_
#define CAN_HAL_H_

#include "can_internal.h"

#include <stdbool.h>
#include <stdint.h>

extern void CAN_HAL_Init(void);
extern void CAN_HAL_TaskIsr(void);
extern bool CAN_HAL_SendMsg(uint32_t id, const uint16_t *data, uint32_t len);
extern bool CAN_HAL_IsMsgPending(void);
extern CAN_Msg CAN_HAL_ReadMsg(void);

#endif /* CAN_HAL_H_ */
