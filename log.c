#include "log.h"

#include "device.h"
#include "driverlib.h"
#include "sci_io_driverlib.h"

#include <file.h>
#include <stdint.h>
#include <stdio.h>

#define LOG_BUF_SIZE 128u

static void GpioConfig(void);
static void SciaConfig(void);

void LOG_Init(void)
{
    GpioConfig();
    SciaConfig();
//    // Redirect STDOUT to SCI
    volatile int status = 0;
    volatile FILE *fid;
    status = add_device("scia",
                        _SSA,
                        SCI_open,
                        SCI_close,
                        SCI_read,
                        SCI_write,
                        SCI_lseek,
                        SCI_unlink,
                        SCI_rename);
    fid = fopen("scia", "w");
    freopen("scia:", "w", stdout);
    setvbuf(stdout, NULL, _IONBF, 0);
    (void)fid;
}

void LOG_DoLog(const char *format, ...)
{
    static int16_t m_step = 0;
    static char m_log_printf_buf[LOG_BUF_SIZE];
    size_t len;

    len = (size_t)snprintf(m_log_printf_buf, LOG_BUF_SIZE - 1u, "%d:", m_step++);

    va_list args; //lint -esym(530, args) Unnecessary to initialize "args"
    va_start(args, format); //lint !e586 Allow deprecated macro "va_start"
    len += (size_t)vsnprintf(&(m_log_printf_buf[len]),
                              (LOG_BUF_SIZE - len) - 1u,
                              format,
                              args);
    if (len >= (LOG_BUF_SIZE - 1u)) {
        len = LOG_BUF_SIZE - 1u;
    }
    va_end(args); //lint !e586 Allow deprecated macro "va_end"
    putchar(0x1B);
    (void)printf("%s\n", m_log_printf_buf);
}

static void GpioConfig(void)
{
    // GPIO28 is the SCI Rx pin.
    GPIO_setPinConfig(DEVICE_GPIO_CFG_SCIRXDA);
    GPIO_setDirectionMode(DEVICE_GPIO_PIN_SCIRXDA, GPIO_DIR_MODE_IN);
    GPIO_setPadConfig(DEVICE_GPIO_PIN_SCIRXDA, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(DEVICE_GPIO_PIN_SCIRXDA, GPIO_QUAL_ASYNC);
    // GPIO29 is the SCI Tx pin.
    GPIO_setPinConfig(DEVICE_GPIO_CFG_SCITXDA);
    GPIO_setDirectionMode(DEVICE_GPIO_PIN_SCITXDA, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(DEVICE_GPIO_PIN_SCITXDA, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(DEVICE_GPIO_PIN_SCITXDA, GPIO_QUAL_ASYNC);
}

static void SciaConfig(void)
{
    // Note: Clocks were turned on to the SCIA peripheral
    // in the InitSysCtrl() function
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_SCIA);

    // 1 stop bit,  No loopback, No parity,8 char bits, async mode,
    // idle-line protocol
    SCI_performSoftwareReset(SCIA_BASE);

    SCI_setConfig(
            SCIA_BASE,
            DEVICE_LSPCLK_FREQ,
            115200,
            (SCI_CONFIG_WLEN_8 | SCI_CONFIG_STOP_ONE | SCI_CONFIG_PAR_NONE));

    SCI_resetChannels(SCIA_BASE);
    //    SCI_resetRxFIFO(SCIA_BASE);
    //    SCI_resetTxFIFO(SCIA_BASE);
    //    SCI_clearInterruptStatus(SCIA_BASE, SCI_INT_TXFF | SCI_INT_RXFF);
    //    SCI_enableFIFO(SCIA_BASE);
    SCI_enableInterrupt(SCIA_BASE, SCI_INT_RXRDY_BRKDT | SCI_INT_TXRDY);
    SCI_enableModule(SCIA_BASE);
    SCI_performSoftwareReset(SCIA_BASE);
}
