#ifndef LOG_H_
#define LOG_H_

extern void LOG_Init(void);
extern void LOG_DoLog(const char *format, ...);

#endif /* LOG_H_ */
