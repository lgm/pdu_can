#include "can_app.h"
#include "can_logic.h"

#include "alarm_manage.h"
#include "target_config.h"

#include <stdbool.h>
#include <string.h>

#define HALL_CURR_TIMEOUT 100
#define BAT_TEMP_TIMEOUT  5000

static int16_t s_curr_100ma[HALL_CURR_NUM] = { 0 };
static bool s_curr_timeout[HALL_CURR_NUM] = { false };
static RELAY_Ctrl s_relay_ctrl;
static DCAR_RELAY_State s_dcar_relay_state;
static bool s_dcar_relay_state_timeout = false;
static BAT_Temp s_bat_temp;
static bool s_bat_temp_timeout = false;
static int32_t s_sample_result[AIN_END] = { 0 };
static uint16_t s_curr_relay_ctrl_state[CA_RELAY_STATE_END] = { 0u };
static uint16_t s_hvil_state = 0u;
static uint16_t s_hv_request_state = 0u;

static void UpdateData(void);
static void SendInfo100ms(void);
static void SendInfo1000ms(void);
#ifdef PDU_UP_CAR
static void SendSampleInfo(void);
static void SendRelayState(void);
static void SendAlarmInfo(void);
static void SendBatSystemInfo(void);
static void SendBatVolt(void);
static void SendBatTemp(void);
static void SendBatVoltHLInfo(void);
static void SendBatTempHLInfo(void);
#else
static void SendDcarRelayState(void);
#endif

void CAN_APP_Init(void)
{
    AM_Init();
    CAN_LOGIC_Init();
    memset(s_curr_100ma, 0, sizeof(s_curr_100ma));
    memset(&s_bat_temp, 0, sizeof(s_bat_temp));
    memset(&s_relay_ctrl, 0, sizeof(s_relay_ctrl));
    memset(&s_dcar_relay_state, 0, sizeof(s_dcar_relay_state));
    (void)s_curr_timeout;
    (void)s_dcar_relay_state_timeout;
    (void)s_bat_temp_timeout;
    memset(s_sample_result, 0, sizeof(s_sample_result));
    memset(s_curr_relay_ctrl_state, 0, sizeof(s_curr_relay_ctrl_state));
    s_hvil_state = 0u;
    s_hv_request_state = 0u;
}

void CAN_APP_Task1ms(void)
{
    AM_Task1ms();
    CAN_LOGIC_Task1ms();
    UpdateData();
    static uint32_t s_timer = 0u;
    s_timer++;
    if ((s_timer % 1000u) == 0u) {
        SendInfo100ms();
        SendInfo1000ms();
    } else if ((s_timer % 100u) == 0u) {
        SendInfo100ms();
    } else {
        // do nothing
    }
}

int16_t CAN_APP_GetCurr100mA(HALL_Curr hall)
{
    return s_curr_100ma[hall];
}

RELAY_Ctrl CAN_APP_GetRelayCtrl(void)
{
    return s_relay_ctrl;
}

DCAR_RELAY_State CAN_APP_GetDcarRelayState(void)
{
    return s_dcar_relay_state;
}

BAT_Temp CAN_APP_GetBatTemp(void)
{
    return s_bat_temp;
}

void CAN_APP_SetSampleResult(CA_AIN_Channel ain_channel, int32_t value)
{
    s_sample_result[ain_channel] = value;
}

int32_t CAN_APP_GetSampleResult(CA_AIN_Channel ain_channel)
{
    return s_sample_result[ain_channel];
}

void CAN_APP_SetRelayState(CA_RELAY_State relay, uint16_t state)
{
    s_curr_relay_ctrl_state[relay] = state;
}

void CAN_APP_SetHVILState(uint16_t state)
{
    s_hvil_state = state;
}

uint16_t CAN_APP_GetHVILState(void)
{
    return s_hvil_state;
}

void CAN_APP_SetHVRequestState(uint16_t state)
{
    s_hv_request_state = state;
}

uint16_t CAN_APP_GetHVRequestState(void)
{
    return s_hv_request_state;
}

static void SendInfo100ms(void)
{
#ifdef PDU_UP_CAR
    SendSampleInfo();
    SendRelayState();
    SendAlarmInfo();
    SendBatSystemInfo();
#else
    SendDcarRelayState();
#endif
}

static void SendInfo1000ms(void)
{
#ifdef PDU_UP_CAR
    SendBatVolt();
    SendBatTemp();
    SendBatVoltHLInfo();
    SendBatTempHLInfo();
#else
    (void)0;
#endif
}

static void UpdateData(void)
{
    // update hall current
    static uint32_t m_curr_timer[HALL_CURR_NUM] = { 0u };
    HALL_Curr i;
    for (i = HALL_CURR_LEFT_BAT; i < HALL_CURR_END; ++i) {
        if (CAN_LOGIC_GetCurr(i, &s_curr_100ma[i])) {
            m_curr_timer[i] = 0u;
        } else {
            m_curr_timer[i]++;
        }
        if (m_curr_timer[i] > HALL_CURR_TIMEOUT) {
            s_curr_timeout[i] = true;
        }
    }
    // update bat temp
    static uint32_t m_temp_timer = 0u;
    if (CAN_LOGIC_GetBatTemp(&s_bat_temp)) {
        m_temp_timer = 0u;
    } else {
        m_temp_timer++;
        if (m_temp_timer > BAT_TEMP_TIMEOUT) {
            s_bat_temp_timeout = true;
        }
    }
    // Update relay control
    (void)CAN_LOGIC_GetRelayCtrl(&s_relay_ctrl);
    // Update dcar relay state, for down car to up car
    static uint32_t m_dcar_relay_state_timer = 0u;
    if (CAN_LOGIC_GetDcarRelayState(&s_dcar_relay_state)) {
        m_dcar_relay_state_timer = 0u;
    } else {
        m_dcar_relay_state_timer++;
        if (m_dcar_relay_state_timer > HALL_CURR_TIMEOUT) {
            s_dcar_relay_state_timeout = true;
        }
    }
}

#ifdef PDU_UP_CAR
static void SendSampleInfo(void)
{
    SAMPLE_Info1 info1;
    info1.bus_volt = (uint16_t)s_sample_result[AIN_BUS_VOLT]; // unit 100mV
    info1.bus_curr = (s_curr_100ma[HALL_CURR_LEFT_BAT]
                      + s_curr_100ma[HALL_CURR_CENT_BAT])
                   + s_curr_100ma[HALL_CURR_RIGHT_BAT]; //  unit 100mA
    info1.x7_curr = s_curr_100ma[HALL_CURR_X7];
    info1.x8_curr = s_curr_100ma[HALL_CURR_X8];
    (void)CAN_LOGIC_SendSampleInfo1(info1);
    SAMPLE_Info2 info2;
    info2.range_extender_curr = s_curr_100ma[HALL_CURR_RANGE_EXTENDER];
    info2.obc_curr = s_curr_100ma[HALL_CURR_OBC];
    info2.fast_a_curr = s_curr_100ma[HALL_CURR_FAST_CHRG_A];
    info2.fast_b_curr = s_curr_100ma[HALL_CURR_FAST_CHRG_B];
    (void)CAN_LOGIC_SendSampleInfo2(info2);
}

static void SendRelayState(void)
{
    static uint16_t time_stamp = 0u;
    RELAY_State state;
    state.left_bat = s_curr_relay_ctrl_state[CA_RELAY_STATE_LEFT_BAT] & 0x03u;
    state.center_bat = s_curr_relay_ctrl_state[CA_RELAY_STATE_CENTER_BAT]
                     & 0x03u;
    state.right_bat = s_curr_relay_ctrl_state[CA_RELAY_STATE_RIGHT_BAT] & 0x03u;
    state.air_condition = s_curr_relay_ctrl_state[CA_RELAY_STATE_AIR_CONDITION]
                        & 0x03u;
    state.x7_x8 = s_dcar_relay_state.x7_x8;
    state.range_extender = s_dcar_relay_state.range_extender;
    state.obc = s_dcar_relay_state.obc;
    state.fast_a = s_dcar_relay_state.fast_chrg_a;
    state.fast_b = s_dcar_relay_state.fast_chrg_b;
    state.time_stamp = time_stamp;
    (void)CAN_LOGIC_SendRealyState(state);
    time_stamp++;
    time_stamp &= 0xFFu;
}

static void SendAlarmInfo(void)
{
    union {
        ALARM_Code code;
        ALARM_Info info;
    } u;
    u.code = AM_GetAndClearAlarmCode();
    (void)CAN_LOGIC_SendAlarmInfo(u.info);
}

static void SendBatSystemInfo(void)
{
    uint16_t volt = (uint16_t)s_sample_result[AIN_LEFT_BAT_VOLT];
    int16_t curr = s_curr_100ma[HALL_CURR_LEFT_BAT];
    if (s_sample_result[AIN_CENTER_BAT_VOLT] > volt) {
        volt = (uint16_t)s_sample_result[AIN_CENTER_BAT_VOLT];
        curr = s_curr_100ma[HALL_CURR_CENT_BAT];
    }
    if (s_sample_result[AIN_RIGHT_BAT_VOLT] > volt) {
        volt = (uint16_t)s_sample_result[AIN_RIGHT_BAT_VOLT];
        curr = s_curr_100ma[HALL_CURR_RIGHT_BAT];
    }
    BAT_SYS_info info;
    info.volt = volt;
    info.curr = curr;
    (void)CAN_LOGIC_SendBatSystemInfo(info);
}

static void SendBatVolt(void)
{
    uint32_t pkt_num = ((274u % 4u) == 0u) ? (274u / 4u) : ((274u / 4u) + 1u);
    uint16_t volt = (uint16_t)s_sample_result[AIN_LEFT_BAT_VOLT]; // Get system
                                                                  // max bat
                                                                  // volt
    if (s_sample_result[AIN_CENTER_BAT_VOLT] > volt) {
        volt = (uint16_t)s_sample_result[AIN_CENTER_BAT_VOLT];
    }
    if (s_sample_result[AIN_RIGHT_BAT_VOLT] > volt) {
        volt = (uint16_t)s_sample_result[AIN_RIGHT_BAT_VOLT];
    }
    volt /= 274u;
    uint32_t i;
    for (i = 0u; i < pkt_num; i++) {
        uint16_t volt_array[4u] = { 0u };
        uint32_t j;
        for (j = 0u; j < 4u; j++) {
            volt_array[j] = volt;
        }
        (void)CAN_LOGIC_SendBatVolt(i * 4u, volt_array);
    }
}

static void SendBatTemp(void)
{
    uint32_t pkt_num = ((274u % 8u) == 0u) ? (274u / 8u) : ((274u / 8u) + 1u);
    int16_t temp = s_bat_temp.left_value;
    if (s_bat_temp.center_value > temp) {
        temp = s_bat_temp.center_value;
    }
    if (s_bat_temp.right_value > temp) {
        temp = s_bat_temp.right_value;
    }
    uint32_t i;
    for (i = 0u; i < pkt_num; i++) {
        int16_t temp_array[8u] = { 0 };
        uint32_t j;
        for (j = 0u; j < 8u; j++) {
            temp_array[j] = temp;
        }
        (void)CAN_LOGIC_SendBatTemp(i * 8u, temp_array);
    }
}

static void SendBatVoltHLInfo(void)
{
    uint16_t left_bat_volt = (uint16_t)s_sample_result[AIN_LEFT_BAT_VOLT];
    uint16_t center_bat_volt = (uint16_t)s_sample_result[AIN_CENTER_BAT_VOLT];
    uint16_t right_bat_volt = (uint16_t)s_sample_result[AIN_RIGHT_BAT_VOLT];
    uint16_t max_volt = left_bat_volt;
    uint16_t max_index = 1u;
    uint16_t min_volt = left_bat_volt;
    uint16_t min_index = 1u;
    // max volt
    if (center_bat_volt > max_volt) {
        max_volt = center_bat_volt;
        max_index = 2u;
    }
    if (right_bat_volt > max_volt) {
        max_volt = right_bat_volt;
        max_index = 3u;
    }
    // min volt
    if (center_bat_volt < min_volt) {
        min_volt = center_bat_volt;
        min_index = 2u;
    }
    if (right_bat_volt < min_volt) {
        min_volt = right_bat_volt;
        min_index = 3u;
    }
    BAT_VOLT_Info volt_info;
    volt_info.high_index = max_index;
    volt_info.high_value = max_volt;
    volt_info.low_index = min_index;
    volt_info.low_value = min_volt;
    (void)CAN_LOGIC_SendBatVoltInfo(volt_info);
}

static void SendBatTempHLInfo(void)
{
    uint16_t left_bat_temp = s_bat_temp.left_value;
    uint16_t center_bat_temp = s_bat_temp.center_value;
    uint16_t right_bat_temp = s_bat_temp.right_value;
    int16_t max_temp = left_bat_temp;
    uint16_t max_index = 1u;
    int16_t min_temp = left_bat_temp;
    uint16_t min_index = 1u;
    // max temp
    if (center_bat_temp > max_temp) {
        max_temp = center_bat_temp;
        max_index = 2u;
    }
    if (right_bat_temp > max_temp) {
        max_temp = right_bat_temp;
        max_index = 3u;
    }
    // min temp
    if (center_bat_temp < min_temp) {
        min_temp = center_bat_temp;
        min_index = 2u;
    }
    if (right_bat_temp < min_temp) {
        min_temp = right_bat_temp;
        min_index = 3u;
    }
    BAT_TEMP_Info temp_info;
    temp_info.high_index = max_index;
    temp_info.high_value = max_temp;
    temp_info.low_index = min_index;
    temp_info.low_value = min_temp;
    (void)CAN_LOGIC_SendBatTempInfo(temp_info);
}

#else
static void SendDcarRelayState(void)
{
    static uint16_t time_stamp = 0u;
    DCAR_RELAY_State state;
    state.x7_x8 = s_curr_relay_ctrl_state[CA_RELAY_STATE_BUS];
    state.range_extender =
            s_curr_relay_ctrl_state[CA_RELAY_STATE_RANGE_EXTENDER];
    state.obc = s_curr_relay_ctrl_state[CA_RELAY_STATE_OBC];
    state.fast_chrg_a = s_curr_relay_ctrl_state[CA_RELAY_STATE_FAST_A];
    state.fast_chrg_b = s_curr_relay_ctrl_state[CA_RELAY_STATE_FAST_B];
    state.time_stamp = time_stamp;
    (void)CAN_LOGIC_SendDcarRelayState(state);
    time_stamp++;
    time_stamp &= 0xFFu;
}
#endif
