#ifndef ALARM_MANAGE_H_
#define ALARM_MANAGE_H_

#include <stdint.h>

typedef struct {
    uint32_t low;
    uint32_t high;
} ALARM_Code;

extern void AM_Init(void);
extern void AM_Task1ms(void);
extern ALARM_Code AM_GetAndClearAlarmCode(void);

#endif /* ALARM_MANAGE_H_ */
