#ifndef CAN_APP_H_
#define CAN_APP_H_

#include "can_internal.h"

#include <stdint.h>

typedef enum {
    AIN_LEFT_BAT_VOLT,
    AIN_CENTER_BAT_VOLT,
    AIN_RIGHT_BAT_VOLT,
    AIN_UP_CAR_PUBLIC_VOLT,
    AIN_RANGE_EXTENDER_VOLT,
    AIN_CHARGE_A_VOLT,
    AIN_CHARGE_B_VOLT,
    AIN_OBC_VOLT,
    AIN_DOWN_CAR_PUBLIC_VOLT,
    AIN_BUS_VOLT,
    AIN_END
} CA_AIN_Channel;

typedef enum {
    CA_RELAY_STATE_LEFT_BAT,
    CA_RELAY_STATE_CENTER_BAT,
    CA_RELAY_STATE_RIGHT_BAT,
    CA_RELAY_STATE_AIR_CONDITION,
    CA_RELAY_STATE_RANGE_EXTENDER,
    CA_RELAY_STATE_FAST_A,
    CA_RELAY_STATE_FAST_B,
    CA_RELAY_STATE_OBC,
    CA_RELAY_STATE_BUS,
    CA_RELAY_STATE_END,
} CA_RELAY_State;

extern void CAN_APP_Init(void);
extern void CAN_APP_Task1ms(void);
extern int16_t CAN_APP_GetCurr100mA(HALL_Curr hall);
extern RELAY_Ctrl CAN_APP_GetRelayCtrl(void); // ���Ƴ�늳�������^������{�ô˺���
extern DCAR_RELAY_State CAN_APP_GetDcarRelayState(void); // ����늳��^����{�ôκ���
extern BAT_Temp CAN_APP_GetBatTemp(void);
// Interface for can and control
// Volt sample
extern void CAN_APP_SetSampleResult(CA_AIN_Channel ain_channel, int32_t value);
extern int32_t CAN_APP_GetSampleResult(CA_AIN_Channel ain_channel);
// Relay control state
extern void CAN_APP_SetRelayState(
        CA_RELAY_State relay,
        uint16_t stasste); // �Д��^�����B�ᣬ�{�ô˺������^�����B���f�ocanģ�K
// HVIL
extern void CAN_APP_SetHVILState(
        uint16_t state); // �Д�߉����i��B�ᣬ�{�ô˺�������B���f�ocanģ�K
extern uint16_t CAN_APP_GetHVILState(void);
// High volt request
extern void CAN_APP_SetHVRequestState(
        uint16_t state); // �Д�߉�Ո���B�ᣬ�{�ô˺������߉�Ո���B���f�ocanģ�K
extern uint16_t CAN_APP_GetHVRequestState(void);

#endif /* CAN_APP_H_ */
