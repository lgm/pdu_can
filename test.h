#ifndef TEST_H_
#define TEST_H_

#include <stdint.h>

typedef enum {
    AIN_BUS_VOLT,
    AIN_BUS_CURR,
    AIN_END
} AIN_CHANNEL;

extern int16_t TEST_GetValue(AIN_CHANNEL ain);

#endif /* TEST_H_ */
