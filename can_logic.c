#include "can_logic.h"
#include "can_hal.h"

#include "CanIf_Cbk.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define HALL_NAME   0x120513u
#define CURR_OFFSET 5000
#define TEMP_OFFSET 40

typedef struct {
    uint32_t name : 24;
    uint32_t error : 1;
    uint32_t error_info : 7;
    uint32_t curr;
} HALL_Info;

static const bool CURR_NORMAL_POSITIVE[HALL_CURR_NUM] = {
    [HALL_CURR_LEFT_BAT] = false,
    [HALL_CURR_CENT_BAT] = false,
    [HALL_CURR_RIGHT_BAT] = false,
    [HALL_CURR_X4] = false,
    [HALL_CURR_X5] = false,
    [HALL_CURR_X6] = true,
    [HALL_CURR_X7] = true,
    [HALL_CURR_X8] = true,
    [HALL_CURR_RANGE_EXTENDER] = true,
    [HALL_CURR_FAST_CHRG_A] = true,
    [HALL_CURR_FAST_CHRG_B] = true,
    [HALL_CURR_OBC] = true,
};

static bool s_curr_pending[HALL_CURR_NUM] = { false };
static int16_t s_pending_curr[HALL_CURR_NUM] = { 0 }; // The unit is 100mA
static bool s_relay_ctrl_pending = false;
static RELAY_Ctrl s_pending_relay_ctrl;
static bool s_dcar_relay_state_pending = false;
static DCAR_RELAY_State s_pending_dcar_relay_state;
static bool s_bat_temp_pending = false;
static BAT_Temp s_pending_bat_temp; // The unit is 1 degree Celsius

static void ProcessMsg(void);
static void OnReceivedHallInfoMsg(CAN_Msg msg);
static void OnReceivedVcuRelayCtrlMsg(CAN_Msg msg);
static void OnReceivedChrgRelayCtrlMsg(CAN_Msg msg);
static void OnReceivedDcarRelayStateMsg(CAN_Msg msg);
static void OnReceivedUpdateMsg(CAN_Msg msg);
static void OnReceivedTempMsg(CAN_Msg msg);
static HALL_Info GetHallInfo(const uint16_t data[]);

void CAN_LOGIC_Init(void)
{
    CAN_HAL_Init();
    memset(s_pending_curr, 0, sizeof(s_pending_curr));
    memset(&s_pending_relay_ctrl, 0, sizeof(s_pending_relay_ctrl));
    memset(&s_pending_dcar_relay_state, 0, sizeof(s_pending_dcar_relay_state));
    memset(&s_pending_bat_temp, 0, sizeof(s_pending_bat_temp));
}

void CAN_LOGIC_Task1ms(void)
{
    ProcessMsg();
}

bool CAN_LOGIC_GetCurr(HALL_Curr hall, int16_t *curr)
{
    bool ret = false;
    if (s_curr_pending[hall]) {
        *curr = s_pending_curr[hall];
        s_curr_pending[hall] = false;
        ret = true;
    }
    return ret;
}

bool CAN_LOGIC_GetRelayCtrl(RELAY_Ctrl *relay_ctrl)
{
    bool ret = false;
    if (s_relay_ctrl_pending) {
        *relay_ctrl = s_pending_relay_ctrl;
        s_relay_ctrl_pending = false;
        ret = true;
    }
    return ret;
}

bool CAN_LOGIC_GetDcarRelayState(DCAR_RELAY_State *relay_state)
{
    bool ret = false;
    if (s_dcar_relay_state_pending) {
        *relay_state = s_pending_dcar_relay_state;
        s_dcar_relay_state_pending = false;
        ret = true;
    }
    return ret;
}

bool CAN_LOGIC_GetBatTemp(BAT_Temp *bat_temp)
{
    bool ret = false;
    if (s_bat_temp_pending) {
        *bat_temp = s_pending_bat_temp;
        s_bat_temp_pending = false;
        ret = true;
    }
    return ret;
}

bool CAN_LOGIC_SendSampleInfo1(SAMPLE_Info1 info)
{
    uint16_t data[8] = { 0u };
    data[0] = info.bus_volt & 0xFFu;
    data[1] = (info.bus_volt >> 8u) & 0xFFu;
    uint16_t curr = (uint16_t)(info.bus_curr + CURR_OFFSET);
    data[2] = curr & 0xFFu;
    data[3] = (curr >> 8u) & 0xFFu;
    curr = (uint16_t)(info.x7_curr + CURR_OFFSET);
    data[4] = curr & 0xFFu;
    data[5] = (curr >> 8u) & 0xFFu;
    curr = (uint16_t)(info.x8_curr + CURR_OFFSET);
    data[6] = curr & 0xFFu;
    data[7] = (curr >> 8u) & 0xFFu;
    return CAN_HAL_SendMsg(CAN_ID_TX_SAMPLE_INFO_1, data, 8u);
}

bool CAN_LOGIC_SendSampleInfo2(SAMPLE_Info2 info)
{
    uint16_t data[8] = { 0u };
    uint16_t curr = (uint16_t)(info.range_extender_curr + CURR_OFFSET);
    data[0] = curr & 0xFFu;
    data[1] = (curr >> 8u) & 0xFFu;
    curr = (uint16_t)(info.obc_curr + CURR_OFFSET);
    data[2] = curr & 0xFFu;
    data[3] = (curr >> 8u) & 0xFFu;
    curr = (uint16_t)(info.fast_a_curr + CURR_OFFSET);
    data[4] = curr & 0xFFu;
    data[5] = (curr >> 8u) & 0xFFu;
    curr = (uint16_t)(info.fast_b_curr + CURR_OFFSET);
    data[6] = curr & 0xFFu;
    data[7] = (curr >> 8u) & 0xFFu;
    return CAN_HAL_SendMsg(CAN_ID_TX_SAMPLE_INFO_2, data, 8u);
}

bool CAN_LOGIC_SendRealyState(RELAY_State state)
{
    uint16_t data[8] = { 0u };
    data[0] = state.left_bat;
    data[0] |= state.center_bat << 2u;
    data[0] |= state.right_bat << 4u;
    data[0] |= state.air_condition << 6u;
    data[0] &= 0xFFu;
    data[1] = state.x7_x8;
    data[1] |= state.range_extender << 2u;
    data[1] |= state.obc << 4u;
    data[1] |= state.fast_a << 6u;
    data[1] &= 0xFFu;
    data[2] = state.fast_b & 0x03u;
    data[3] = state.time_stamp & 0xFFu;
    return CAN_HAL_SendMsg(CAN_ID_TX_RELAY_STATE, data, 8u);
}

bool CAN_LOGIC_SendDcarRelayState(DCAR_RELAY_State state)
{
    uint16_t data[8] = { 0u };
    data[0] = state.x7_x8;
    data[0] |= state.range_extender << 2u;
    data[0] |= state.obc << 4u;
    data[0] |= state.fast_chrg_a << 6u;
    data[0] &= 0xFFu;
    data[1] = state.fast_chrg_b & 0x03u;
    data[2] = state.time_stamp & 0xFFu;
    return CAN_HAL_SendMsg(CAN_ID_DCAR_RELAY_STATE, data, 8u);
}

bool CAN_LOGIC_SendAlarmInfo(ALARM_Info info)
{
    uint16_t data[8] = { 0 };
    data[0] = info.alarm_level;
    data[0] |= info.over_volt << 2u;
    data[0] |= info.under_volt << 4u;
    data[0] |= info.bat_volt_diff << 6u;
    data[0] &= 0xFFu;
    data[1] = info.hvil;
    data[1] |= info.bat_uniformity << 2u;
    data[1] |= info.left_bat_ov << 4u;
    data[1] |= info.center_bat_ov << 6u;
    data[1] &= 0xFFu;
    data[2] = info.right_bat_ov;
    data[2] |= info.left_bat_uv << 2u;
    data[2] |= info.center_bat_uv << 4u;
    data[2] |= info.right_bat_uv << 6u;
    data[2] &= 0xFFu;
    data[3] = info.left_bat_ut;
    data[3] |= info.center_bat_ut << 2u;
    data[3] |= info.right_bat_ut << 4u;
    data[3] |= info.left_bat_ot << 6u;
    data[3] &= 0xFFu;
    data[4] = info.center_bat_ot;
    data[4] |= info.right_bat_ot << 2u;
    data[4] |= info.left_bat_vd << 4u;
    data[4] |= info.center_bat_vd << 6u;
    data[4] &= 0xFFu;
    data[5] = info.right_bat_vd;
    data[5] |= info.left_bat_td << 2u;
    data[5] |= info.center_bat_td << 4u;
    data[5] |= info.right_bat_td << 6u;
    data[5] &= 0xFFu;
    data[6] = info.hv_request & 0x01u;
    return CAN_HAL_SendMsg(CAN_ID_TX_ALARMS, data, 8u);
}

bool CAN_LOGIC_SendBatSystemInfo(BAT_SYS_info info)
{
    uint16_t data[8] = { 0u };
    data[0] = info.volt & 0xFFu;
    data[1] = (info.volt >> 8u) & 0xFFu;
    uint16_t curr = (uint16_t)(info.curr + CURR_OFFSET);
    data[2] = curr & 0xFFu;
    data[3] = (curr >> 8u) & 0xFFu;
    return CAN_HAL_SendMsg(CAN_ID_TX_BAT_SYSTEM_INFO, data, 8u);
}

// The index is the fisrt bat index, the can divisible by 4
bool CAN_LOGIC_SendBatVolt(uint16_t index, uint16_t volt[])
{
    uint16_t data[8] = { 0u };
    data[0] = volt[0] & 0xFFu;
    data[1] = (volt[0] >> 8u) & 0xFFu;
    data[2] = volt[1] & 0xFFu;
    data[3] = (volt[1] >> 8u) & 0xFFu;
    data[4] = volt[2] & 0xFFu;
    data[5] = (volt[2] >> 8u) & 0xFFu;
    data[6] = volt[3] & 0xFFu;
    data[7] = (volt[3] >> 8u) & 0xFFu;
    uint32_t id = (CAN_ID_TX_BAY_SYSTEM_VOLT >> 16u) + (index / 4u);
    id <<= 16u;
    id |= (CAN_ID_TX_BAY_SYSTEM_VOLT & 0xFFFF);
    return CAN_HAL_SendMsg(id, data, 8u);
}

// The index is the fisrt bat index, the can divisible by 8
bool CAN_LOGIC_SendBatTemp(uint16_t index, int16_t temp[])
{
    uint16_t data[8] = { 0u };
    data[0] = (uint16_t)(temp[0] + TEMP_OFFSET) & 0xFFu;
    data[1] = (uint16_t)(temp[1] + TEMP_OFFSET) & 0xFFu;
    data[2] = (uint16_t)(temp[2] + TEMP_OFFSET) & 0xFFu;
    data[3] = (uint16_t)(temp[3] + TEMP_OFFSET) & 0xFFu;
    data[4] = (uint16_t)(temp[4] + TEMP_OFFSET) & 0xFFu;
    data[5] = (uint16_t)(temp[5] + TEMP_OFFSET) & 0xFFu;
    data[6] = (uint16_t)(temp[6] + TEMP_OFFSET) & 0xFFu;
    data[7] = (uint16_t)(temp[7] + TEMP_OFFSET) & 0xFFu;
    uint32_t id = (CAN_ID_TX_BAT_SYSTEM_TEMP >> 16u) + (index / 8u);
    id <<= 16u;
    id |= (CAN_ID_TX_BAT_SYSTEM_TEMP & 0xFFFF);
    return CAN_HAL_SendMsg(id, data, 8u);
}

bool CAN_LOGIC_SendBatVoltInfo(BAT_VOLT_Info info)
{
    uint16_t data[8] = { 0u };
    data[0] = info.high_index & 0xFFu;
    data[1] = 0u;
    data[2] = info.high_value & 0xFFu;
    data[3] = (info.high_value >> 8u) & 0xFFu;
    data[4] = info.low_index & 0xFFu;
    data[5] = 0u;
    data[6] = info.low_value & 0xFFu;
    data[7] = (info.low_value >> 8u) & 0xFFu;
    return CAN_HAL_SendMsg(CAN_ID_TX_BAT_VOLT_INFO, data, 8u);
}

bool CAN_LOGIC_SendBatTempInfo(BAT_TEMP_Info info)
{
    uint16_t data[8] = { 0u };
    data[0] = info.high_index & 0xFFu;
    data[1] = (uint16_t)(info.high_value + TEMP_OFFSET) & 0xFFu;
    data[2] = info.low_index;
    data[3] = (uint16_t)(info.low_value + TEMP_OFFSET) & 0xFFu;
    return CAN_HAL_SendMsg(CAN_ID_TX_BAT_TEMP_INFO, data, 8u);
}

static void ProcessMsg(void)
{
    if (CAN_HAL_IsMsgPending()) {
        CAN_Msg msg = CAN_HAL_ReadMsg();
        switch (msg.id) {
        case CAN_ID_RX_HALL_LEFT_BAT:
        case CAN_ID_RX_HALL_CENT_BAT:
        case CAN_ID_RX_HALL_RIGHT_BAT:
        case CAN_ID_RX_HALL_X4:
        case CAN_ID_RX_HALL_X5:
        case CAN_ID_RX_HALL_X6:
        case CAN_ID_RX_HALL_X7:
        case CAN_ID_RX_HALL_X8:
        case CAN_ID_RX_HALL_RANGE_EXTENDER:
        case CAN_ID_RX_HALL_FAST_CHRG_A:
        case CAN_ID_RX_HALL_FAST_CHRG_B:
        case CAN_ID_RX_HALL_OBC:
            OnReceivedHallInfoMsg(msg);
            break;
        case CAN_ID_RX_RELAY_CTRL_VCU:
            OnReceivedVcuRelayCtrlMsg(msg);
            break;
        case CAN_ID_RX_RELAY_CTRL_CHRG:
            OnReceivedChrgRelayCtrlMsg(msg);
            break;
        case CAN_ID_DCAR_RELAY_STATE: // Down car use dummy ID, so it can't be
                                      // received
            OnReceivedDcarRelayStateMsg(msg);
            break;
        case CAN_ID_FW_WCT:
        case CAN_ID_FW_PHYSICS:
        case CAN_ID_FW_FUN:
            OnReceivedUpdateMsg(msg);
            break;
        default:
            if ((msg.id & 0xFFFFFF00u) == CAN_ID_RX_TEMP_DETECT) {
                OnReceivedTempMsg(msg);
            } else {
                // Do nothing
            }
            break;
        }
    }
}

static void OnReceivedHallInfoMsg(CAN_Msg msg)
{
    uint32_t index = msg.id - CAN_ID_RX_HALL_START_ID;
    HALL_Info curr_info = GetHallInfo(msg.data);
    if ((curr_info.error == 0u) && (curr_info.name == HALL_NAME)) {
        if (curr_info.curr >= 0x80000000) {
            s_pending_curr[index] =
                    (int16_t)(curr_info.curr - 0x80000000u) / 100; // 100mA
        } else {
            s_pending_curr[index] =
                    -(int16_t)(0x80000000u - curr_info.curr) / 100; // 100mA
        }
        if (CURR_NORMAL_POSITIVE[index]) {
            s_pending_curr[index] = abs(s_pending_curr[index]);
        }
        s_curr_pending[index] = true;
    }
}

static void OnReceivedVcuRelayCtrlMsg(CAN_Msg msg)
{
    s_pending_relay_ctrl.x7_x8 = msg.data[0] & 0x01u;
    s_pending_relay_ctrl.range_extender = (msg.data[0] >> 2u) & 0x01u;
    s_pending_relay_ctrl.air_condition = (msg.data[0] >> 3u) & 0x01u;
    s_relay_ctrl_pending = true;
}

static void OnReceivedChrgRelayCtrlMsg(CAN_Msg msg)
{
    s_pending_relay_ctrl.fast_chrg_a = msg.data[0] & 0x01u;
    s_pending_relay_ctrl.fast_chrg_b = (msg.data[0] >> 1u) & 0x01u;
    s_pending_relay_ctrl.obc = (msg.data[0] >> 2u) & 0x01u;
    s_relay_ctrl_pending = true;
}

static void OnReceivedDcarRelayStateMsg(CAN_Msg msg)
{
    s_pending_dcar_relay_state.x7_x8 = msg.data[0] & 0x03u;
    s_pending_dcar_relay_state.range_extender = (msg.data[0] >> 2u) & 0x03u;
    s_pending_dcar_relay_state.obc = (msg.data[0] >> 4u) & 0x03u;
    s_pending_dcar_relay_state.fast_chrg_a = (msg.data[0] >> 6u) & 0x03u;
    s_pending_dcar_relay_state.fast_chrg_b = msg.data[1] & 0x03u;
    s_pending_dcar_relay_state.time_stamp = msg.data[2] & 0xFFu;
    s_dcar_relay_state_pending = true;
}

static void OnReceivedUpdateMsg(CAN_Msg msg)
{
    CanIf_RxIndication(0, msg.id, 8, msg.data);
}

static void OnReceivedTempMsg(CAN_Msg msg)
{
    s_pending_bat_temp.left_value =
            (int16_t)(msg.data[0] & 0xFFu) - TEMP_OFFSET;
    s_pending_bat_temp.center_value =
            (int16_t)(msg.data[1] & 0xFFu) - TEMP_OFFSET;
    s_pending_bat_temp.right_value =
            (int16_t)(msg.data[2] & 0xFFu) - TEMP_OFFSET;
    s_bat_temp_pending = true;
}

static HALL_Info GetHallInfo(const uint16_t data[])
{
    HALL_Info info;
    info.curr = (uint32_t)(data[0] & 0xFFu) << 24u;
    info.curr |= (uint32_t)(data[1] & 0xFFu) << 16u;
    info.curr |= (uint32_t)(data[2] & 0xFFu) << 8u;
    info.curr |= (uint32_t)(data[3] & 0xFFu);
    info.error = (uint32_t)data[4] & 0x0001u;
    info.error_info = (uint32_t)(data[4] >> 1u) & 0x007Fu;
    info.name = (uint32_t)data[5] << 16u;
    info.name |= (uint32_t)data[6] << 8u;
    info.name |= (uint32_t)data[7];
    return info;
}
