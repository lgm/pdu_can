#include "test.h"
#include <stdint.h>
#include <stdio.h>

#include "alarm_manage.h"

static int16_t s_ain_result[AIN_END] = { 0 };

static void Task1ms(void);

int main(void)
{
    s_ain_result[AIN_BUS_VOLT] = 700;
    s_ain_result[AIN_BUS_CURR] = 1000;
    AM_Init();
    printf("Hello, world!\n");
    Task1ms();
    return 0;
}

int16_t TEST_GetValue(AIN_CHANNEL ain)
{
    return s_ain_result[ain];
}

static void Task1ms(void)
{
    uint32_t i = 30000;
    do {
        // AM_Task1ms();
        // ALARM_Code code = AM_GetAndClearAlarmCode();
        // if ((code.low != 0) || (code.high != 0)) {
        //     printf("ALARM: %04x%04X\n", code.high, code.low);
        // }
    } while (--i);
    printf("end\n");
}
