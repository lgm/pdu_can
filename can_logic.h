#ifndef CAN_LOGIC_H_
#define CAN_LOGIC_H_

#include "can_internal.h"

#include <stdbool.h>
#include <stdint.h>

extern void CAN_LOGIC_Init(void);
extern void CAN_LOGIC_Task1ms(void);
extern bool CAN_LOGIC_GetCurr(HALL_Curr hall, int16_t *curr);
extern bool CAN_LOGIC_GetRelayCtrl(RELAY_Ctrl *relay_ctrl);
extern bool CAN_LOGIC_GetDcarRelayState(DCAR_RELAY_State *relay_state);
extern bool CAN_LOGIC_GetBatTemp(BAT_Temp *bat_temp);
extern bool CAN_LOGIC_SendSampleInfo1(SAMPLE_Info1 info);
extern bool CAN_LOGIC_SendSampleInfo2(SAMPLE_Info2 info);
extern bool CAN_LOGIC_SendRealyState(RELAY_State state);
extern bool CAN_LOGIC_SendDcarRelayState(DCAR_RELAY_State state);
extern bool CAN_LOGIC_SendAlarmInfo(ALARM_Info info);
extern bool CAN_LOGIC_SendBatSystemInfo(BAT_SYS_info info);
extern bool CAN_LOGIC_SendBatVolt(uint16_t index, uint16_t volt[]);
extern bool CAN_LOGIC_SendBatTemp(uint16_t index, int16_t temp[]);
extern bool CAN_LOGIC_SendBatVoltInfo(BAT_VOLT_Info info);
extern bool CAN_LOGIC_SendBatTempInfo(BAT_TEMP_Info info);

#endif /* CAN_LOGIC_H_ */
